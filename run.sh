#!/bin/bash
if [ $# -eq 0 ]
then
    echo usage $0 '<number>'
    exit 1
fi
IO=~colnet5/IO
t=$1
if ! cat ${IO}/input.${t} > /dev/null
then
    exit 1    
fi
if ! java -ea Main < ${IO}/input.${t} >& output.${t}
then
    echo tail output.${t}
    exit 1
fi
if ! diff ${IO}/output.${t} output.${t} >& /dev/null
then
    echo diff ${IO}/output.${t} output.${t} 
    exit 1
fi
rm output.${t}
echo good
