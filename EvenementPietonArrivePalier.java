public class EvenementPietonArrivePalier extends Evenement {
    /* PAP: Pieton Arrive Palier
       L'instant précis ou un passager qui à décidé de continuer à pieds arrive sur un palier donné.
       Classe à faire complètement par vos soins.
    */

    public void afficheDetails(StringBuilder buffer, Immeuble immeuble) {
	notYetImplemented();
    }

    public void traiter(Immeuble immeuble, Echeancier echeancier) {
	notYetImplemented();
    }

    public EvenementPietonArrivePalier(long d) {
	// Signature approximative et temporaire... juste pour que cela compile.
	super(d);
	notYetImplemented();
    }
    
}
