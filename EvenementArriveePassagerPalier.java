public class EvenementArriveePassagerPalier extends Evenement {
    /* APP: Arrivée Passager Palier
       L'instant précis ou un nouveau passager arrive sur un palier donné.
    */

    private Etage étage;

    public EvenementArriveePassagerPalier(long d, Etage edd) {
	super(d);
	étage = edd;
    }

    public void afficheDetails(StringBuilder buffer, Immeuble immeuble) {
	buffer.append("APP ");
	buffer.append(étage.numéro());
    }

    public void traiter(Immeuble immeuble, Echeancier echeancier) {
    	assert étage != null;
    	assert immeuble.étage(étage.numéro()) == étage;
    	Passager p = new Passager(date, étage, immeuble);
      étage.ajouter(p);
      if (étage.immeuble().cabine.intention() == '-') {
        étage.immeuble().cabine.changerIntention(immeuble.cabine.diff(p));
      }
      if (p.intention() == étage.immeuble().cabine.intention()) { ///// pas bon a changer dès que ça marche
        if (p.étageDépart().getNum() == immeuble.cabine.étage.getNum()) {
          étage.immeuble().cabine.faireMonterPassager(p);
          étage.remove(p);
        } else {
          if (immeuble.cabine.intention() == '-') { // peut etre inutile pareil que debut
            immeuble.cabine.changerIntention(immeuble.cabine.diff(p));
          }
        }
        echeancier.ajouter(new EvenementFermeturePorteCabine(date + Global.tempsPourOuvrirOuFermerLesPortes + Global.tempsPourEntrerOuSortirDeLaCabine));
        echeancier.ajouter(new EvenementArriveePassagerPalier(date + étage.arrivéeSuivante(), étage));
      }
	//notYetImplemented();
    }

}
